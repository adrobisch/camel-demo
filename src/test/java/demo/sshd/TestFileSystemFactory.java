package demo.sshd;

import org.apache.sshd.common.Session;
import org.apache.sshd.server.FileSystemView;
import org.apache.sshd.server.filesystem.NativeFileSystemFactory;
import org.apache.sshd.server.filesystem.NativeFileSystemView;

public class TestFileSystemFactory extends NativeFileSystemFactory {
    public FileSystemView createFileSystemView(final Session session) {
        return new NativeFileSystemView(session.getUsername(), isCaseInsensitive()) {
        };
    }
}