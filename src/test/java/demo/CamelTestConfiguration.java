package demo;

import com.jcraft.jsch.*;
import demo.sshd.TestFileSystemFactory;
import org.apache.camel.spring.CamelContextFactoryBean;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.sshd.SshServer;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.UserAuth;
import org.apache.sshd.server.auth.UserAuthNone;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.sftp.SftpSubsystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Configuration
public class CamelTestConfiguration {

    @Autowired
    ApplicationContext context;

    @Bean
    @DependsOn("sftpServer")
    public SpringCamelContext camelContext() throws Exception {
        CamelContextFactoryBean camelContextFactoryBean = new CamelContextFactoryBean();
        camelContextFactoryBean.setApplicationContext(context);
        camelContextFactoryBean.setPackages(new String[]{"demo.route"});
        camelContextFactoryBean.setId("camelTestContext");
        camelContextFactoryBean.afterPropertiesSet();
        return camelContextFactoryBean.getObject();
    }

    @Bean
    public Integer sftpPort() {
        return 5022;
    }

    @Bean
    public SshServer sftpServer() throws IOException, SftpException, JSchException {
        SshServer sshd = SshServer.setUpDefaultServer();
        sshd.setPort(sftpPort());
        sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider("hostkey.ser"));

        List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<NamedFactory<UserAuth>>();
        userAuthFactories.add(new UserAuthNone.Factory());
        sshd.setUserAuthFactories(userAuthFactories);

        sshd.setCommandFactory(new ScpCommandFactory());
        sshd.setFileSystemFactory(new TestFileSystemFactory());

        List<NamedFactory<Command>> namedFactoryList = new ArrayList<NamedFactory<Command>>();
        namedFactoryList.add(new SftpSubsystem.Factory());
        sshd.setSubsystemFactories(namedFactoryList);

        sshd.start();

        return sshd;
    }

    public void uploadTestFile(String testFileName) throws JSchException, SftpException {
        JSch jsch = new JSch();

        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        JSch.setConfig(config);

        Session session = jsch.getSession("remote-username", "localhost", sftpPort());
        session.setPassword("remote-password");

        session.connect();

        Channel channel = session.openChannel("sftp");
        channel.connect();

        ChannelSftp sftpChannel = (ChannelSftp) channel;
        String uploadedFileName = "testFile";
        sftpChannel.put(getFileStream(testFileName), uploadedFileName);

        if (sftpChannel.isConnected()) {
            sftpChannel.exit();
        }

        if (session.isConnected()) {
            session.disconnect();
        }
    }

    public InputStream getFileStream(String file) {
        return getClass().getClassLoader().getResourceAsStream(file);
    }
}
