package demo.route;

import demo.csv.TestRecord;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class TestRouteBuilder extends SpringRouteBuilder {

  @Override
  public void configure() throws Exception {
    from("sftp://remote-username@127.0.0.1:5022/src/test/resources")
           .unmarshal().zipFile()
           .unmarshal(new BindyCsvDataFormat("demo.csv"))
           .split(body()).streaming().process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(((HashMap) exchange.getIn().getBody()).get(TestRecord.class.getName()));
                }
            })
           .to("mock:result")
           .to("log:test");
  }

}