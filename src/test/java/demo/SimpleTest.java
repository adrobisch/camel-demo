package demo;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static demo.util.FileUtil.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SimpleTest extends CamelTestSupport {

  @EndpointInject(uri="mock:testEndPoint")
  MockEndpoint testEndPoint;

  @EndpointInject(uri="mock:afterProcessor")
  MockEndpoint afterProcessor;

  private String inputDirPath;
  private static final String tempDir = "input";

  @Before
  public void before() throws IOException {
    inputDirPath = setupTempDirectory(tempDir);
    writeIntoFile(inputDirPath, "testFile", "testContent");
  }

  @Test
  public void fileRouteShouldWriteToErrorFolder() {
    testEndPoint.expectedBodiesReceived("testContent");
    afterProcessor.expectedMessageCount(0);
  }

  @Override
  protected RouteBuilder createRouteBuilder() throws Exception {
    return new RouteBuilder() {
      @Override
      public void configure() throws Exception {
        from("file:"+ tempDirPath(tempDir)+"?moveFailed=/tmp/error")
          .multicast().to("direct:processWithError", "mock:testEndPoint");

        from("direct:processWithError")
          .process(new Processor() {
          @Override
          public void process(Exchange exchange) throws Exception {
            throw new RuntimeException();
          }
        }).to("mock:afterProcessor");
      }
    };
  }

  @Override
  public String isMockEndpoints() {
    return "*";
  }

  @After
  public void after() throws InterruptedException {
    assertMockEndpointsSatisfied(5, TimeUnit.SECONDS);
  }

}
