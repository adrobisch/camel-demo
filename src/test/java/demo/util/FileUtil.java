package demo.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtil {
  public static void recursiveDelete(File rootDir) {
    recursiveDelete(rootDir, true);
  }

  public static void recursiveDelete(File rootDir, boolean deleteRoot) {
    File[] childDirs = rootDir.listFiles();
    for(int i = 0; i < childDirs.length; i++) {
      if(childDirs[i].isFile()) {
        childDirs[i].delete();
      }
      else {
        recursiveDelete(childDirs[i], deleteRoot);
        childDirs[i].delete();
      }
    }

    if(deleteRoot) {
      rootDir.delete();
    }
  }

  public static String setupTempDirectory(String dirName) {
    String inputDirPath = tempDirPath(dirName);
    recursiveDelete(new File(inputDirPath));
    new File(inputDirPath).mkdir();
    return inputDirPath;
  }

  public static String tempDirPath(String subDir) {
    String tempDir = System.getProperty("java.io.tmpdir");
    return tempDir+ File.separatorChar + "input";
  }

  public static String writeIntoFile(String path, String fileName, String content) throws IOException {
    FileOutputStream testFileStream = new FileOutputStream(new File(path+File.separatorChar+fileName));
    testFileStream.write(content.getBytes());
    testFileStream.flush();
    testFileStream.close();
    return content;
  }

}