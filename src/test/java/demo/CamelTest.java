package demo;

import demo.csv.TestRecord;
import org.apache.camel.CamelContext;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=CamelTestConfiguration.class)
public class CamelTest {

  @Autowired
  private CamelContext camelContext;

  @Test
  public void testCamel() throws InterruptedException {
    Assert.assertEquals(1, camelContext.getRoutes().size());
    MockEndpoint mockResult = getMockEndpoint("mock:result");
    mockResult.expectedBodiesReceived(new TestRecord("1A", "1B", "1C"), new TestRecord("2A", "2B", "2C"), new TestRecord("3A", "3B", "3C"));
    mockResult.assertIsSatisfied();
  }

  private MockEndpoint getMockEndpoint(String uri) {
    return (MockEndpoint) camelContext.getEndpoint(uri);
  }

}
